#!/bin/sh

#extrai o tempo de execução da ultima thread a rodar o programa em cada nó
num_maq=7
echo "inicio for $num_maq"
for ((a=2; a<=$num_maq-2; a++)); do
   	c="$num_maq"
   	b="testes$a.txt"
   	nome_arq="$c$b"
 	echo "$nome_arq"

	awk 'NR==27' "$nome_arq" >> "result$num_maq.txt"
done

echo "fim for"
